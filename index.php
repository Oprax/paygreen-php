<?php

session_start();

use GuzzleHttp\Exception\RequestException;
use Oprax\Paygreen\Payment;

require("vendor/autoload.php");

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$paygreen = new Payment(getenv("PAYGREEN_ID"), getenv("PAYGREEN_SECRET"), "https://preprod.paygreen.fr");
try {
    var_dump($paygreen->getPaymentTypeAvailable());
    $orderId = uniqid('TEST_');
    $resp = $paygreen->createInstantPayment(
        $orderId,
        500,
        "TRD",
        "https://78fb953fe726.ngrok.io/return.php",
        [
            "id" => "d9b7b0fa",
            "lastName" => "Pay",
            "firstName" => "Green",
            "email" => "contact@paygreen.fr",
            "country" => "FR",
        ],
        "https://78fb953fe726.ngrok.io/notif.php",
    );
    var_dump($resp);
    if ($resp->success) {
        $_SESSION["pid"] = $resp->data->id;
        $_SESSION["orderId"] = $orderId;
        echo "<a href=\"{$resp->data->url}\">pay</a>";
    }
} catch (RequestException $e) {
    var_dump($e->getMessage());
    $req = $e->getRequest();
    var_dump($req->getHeaders(), $req->getUri());
    if ($e->hasResponse()) {
        $resp = $e->getResponse();
        var_dump($resp->getStatusCode(), $resp->getHeaders(), $resp->getBody()->getContents());
    }
}
