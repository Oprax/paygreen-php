<?php

namespace Oprax\Paygreen\Exceptions;

class OrderIdMismatchException extends PaygreenException {}
