<?php

namespace Oprax\Paygreen\Exceptions;

class PaymentNotAvailableException extends PaygreenException {}
