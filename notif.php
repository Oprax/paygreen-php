<?php

session_start();

use Oprax\Paygreen\Payment;

require("vendor/autoload.php");

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

ob_start();
var_dump($_SERVER['REQUEST_METHOD'], $_GET, $_POST, $_SESSION);

$orderId = $_SESSION["orderId"];

if (isset($_GET) && isset($_GET["pid"])) {
    $paygreen = new Payment(getenv("PAYGREEN_ID"), getenv("PAYGREEN_SECRET"), "https://preprod.paygreen.fr");

    $pid = $_SESSION["pid"];
    if ($pid) {
        $resp = $paygreen->confirmPayment($pid);
        if ($resp) {
            echo "GOOD";
        } else {
            echo "NOPE";
        }
    } else {
        echo "NOPE";
    }
}


$content = ob_get_clean();
file_put_contents("log-$orderId.html", $content);

